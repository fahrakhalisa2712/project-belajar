import React, { useState } from "react";
import "./App.css";
import { TextField, Button } from "@material-ui/core";

function App() {
  const [text, setText] = useState("");
  const [hasil, setHasil] = useState("");

  const handleChange = (event) => {
    setText(event.target.value);
  };
  const handleButton = () => {
    setHasil(text);
  };

  return (
    <div className="App">
      <TextField
        value={text}
        onChange={handleChange}
        id="outlined-basic"
        label="Outlined"
        variant="outlined"
      />
      <br />
      <br/>
      <Button onClick={handleButton} variant="contained" color="primary">
        Hasil
      </Button>
      <br />
      <p>{hasil}</p>
    </div>
  );
}

export default App;
